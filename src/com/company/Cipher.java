package com.company;

public interface Cipher {

    /** szyfrowanie wiadomosci */
    String encode(final String message);

    String decode(final String message);
}
