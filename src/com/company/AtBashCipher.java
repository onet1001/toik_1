package com.company;

public class AtBashCipher implements Cipher{
    @Override
    public String encode(String message) {
        StringBuilder ciphertext = new StringBuilder();

        for(char c : message.toCharArray())
        {
            if(Character.isLetter(c))
            {
                if(Character.isUpperCase(c))
                    ciphertext.append((char) ('A' + ('Z' - c)));
               if(Character.isLowerCase(c))
                   ciphertext.append((char) ('a' + ('z' - c)));
            }
            else
            {
                ciphertext.append(c);
            }
        }
        return (ciphertext.toString());
    }

    @Override
    public String decode(String message) {
        return(encode(message));
    }

}
